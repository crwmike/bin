#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      xkey
# Description:      Show key codes using xev
# Dependencies:     xev
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
# xev - print contents of X events  
#
# Xev creates a window and then asks the X server to send it events whenever 
# anything happens to the window (such as it being moved, resized, typed in, 
# clicked in, etc.). You can also attach it to an existing window. It is useful 
# for seeing what causes events to occur and to display the information that 
# they contain; it is essentially a debugging and development tool, and should 
# not be needed in normal usage.  
#
# Read more at: https://www.commandlinux.com/man-page/man1/xev.1.html
#-------------------------------------------------------------------------------
help_text() {
    echo -e "\e[97mXKEY\e[0m(\e[94mxev\e[0m)"
    echo
    echo -e "Usage: xkey [\e[92mOPTION\e[0m]"
    echo
    echo -e "    \e[92m<none>         \e[0mshow key codes for the keys pressed"
    echo -e "    \e[92m-h, --help     \e[0mprints this help"
    echo
}

if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
    help_text
    exit
fi

xev | awk -F'[ )]+' '/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'
