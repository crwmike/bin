#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      tops
# Description:      Menu of Linux top like CLI/TUI app menu.
# Dependencies:     atop, btm, btop, cointop, glances, gotop, htop, iotop, nmon,
#                   top, vtop, ytop, zenith, zfxtop
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
no_cmd() {
    echo "tops:

    =================================
     $(date)
    =================================
     ERROR! selection #${run_me}
     command or file not found!
    =================================

    " | less -e
}

sudo_message() {
    echo -e "\n  \e[92m#####################\n  ##  \e[97mSUDO PASSWORD  \e[92m##\n  #####################\n\e[0m"
}

# simple menu
# HEIGHT=CHOICE_HEIGHT+7
HEIGHT=20
WIDTH=80
CHOICE_HEIGHT=14
BACKTITLE="Linux top like CLI/TUI apps..."
TITLE="top like apps"
MENU="Choose one of the following options:"

OPTIONS=(1 "atop        (ROOT) A CLI monitoring tool for Linux."
         2 "btm         bottom in full mode."
         3 "btm -b      bottom hides graphs and uses a more basic look."
         4 "btop++      C++ version and continuation of bashtop and bpytop."
         5 "glances     A cross platform curses-based monitoring tool."
         6 "htop        An interactive process viewer for Linux."
         7 "iotop       Monitor Linux disk I/O activity and usage."
         8 "nmon        Analyze and monitor linux system performance."
         9 "nvtop       A (h)top like task monitor for GPUs"
         A "top         Displays processor activity of your Linux system."
         B "vtop        A grapical activity monitor for the command line."
         C "zenith      In terminal graphical metrics written in Rust."
         Q "quit        Quit this menu.")

while true;
do
    CHOICE=$(dialog --clear \
                    --backtitle "$BACKTITLE" \
                    --title "$TITLE" \
                    --menu "$MENU" \
                    $HEIGHT $WIDTH $CHOICE_HEIGHT \
                    "${OPTIONS[@]}" \
                    2>&1 >/dev/tty)

    clear
    case $CHOICE in
        1) sudo_message; sudo atop -y || no_cmd ;;
        2) btm || no_cmd ;;
        3) btm -b || no_cmd ;;
        4) btop || no_cmd ;;
        5) glances --process-short-name || no_cmd ;;
        6) htim || htop || no_cmd ;;
        7) sudo_message; sudo iotop || no_cmd ;;
        8) nmon || no_cmd ;;
        9) nvtop || no_cmd ;;
        A) top || no_cmd ;;
        B) vtop -t gooey || no_cmd ;;
        C) zenith || no_cmd ;;
        Q) break ;;
    esac
done
