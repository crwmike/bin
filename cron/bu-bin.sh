#!/bin/bash
#-------------------------------------------------------------------------------
# Script Name:      bu-bin.sh
# Description:      Backup  ~/bin/* 
#                           ~/.bin*
#                           ~/.local/bin/*
#                   Dest:   /run/media/mike/ubackup/bin/
#                           /run/media/mike/ubackup/dot-bin/
#                           /run/media/mike/ubackup/local-bin/
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
cp -rf /home/mike/bin/* /run/media/mike/ubackup/bin/
cp -rf /home/mike/.bin/* /run/media/mike/ubackup/dot-bin/
cp -rf /home/mike/.local/bin/* /run/media/mike/ubackup/local-bin/

echo "[$(date)] 'bu-bin.sh': backup ~/bin, ~/.local/bin" >> /home/mike/.local/log/cron.log

