#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      tldr-update.sh
# Description:      Update tldr's local cache.
# Dependencies:     tldr
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------

/usr/bin/tldr --update
echo "[$(date)] 'tldr-update.sh': update tldr local cache" >> /home/mike/.local/log/cron.log

