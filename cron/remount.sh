#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      remount.sh
# Description:      Make sure /dev/sdb2 is mounted to /run/media/mike/ubackup/
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
# chek if run as root
if (( "$EUID" != 0 )); then
    echo "Please run as root"
    exit
fi

umount /run/media/mike/ubackup*
notify-send "remount.sh:" "$(date)\n\nUnmounting /run/media/mike/ubackup[1..9]"

mkdir -p /run/media/mike/ubackup
mount -t ext4 /dev/sdb2 /run/media/mike/ubackup
notify-send "remount.sh:" "$(date)\n\nMounting /run/media/mike/ubackup"

