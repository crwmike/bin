#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# File:             clean-tmp.sh
# Description:      Delete files in ~/tmp older than 15 days
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
DFILES=$(find ~/tmp/ -mtime +15 -type f)
sleep 5

find ~/tmp/ -mtime +15 -type f -delete

echo "[$(date)] 'clean-tmp.sh': delete old files (~/tmp)" >> /home/mike/.local/log/cron.log

