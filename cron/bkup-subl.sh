#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      bkup-subl.sh
# Description:      Backup Sublime Text 3 package files
# Dependencies:     Sublime Text 3
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
cp -rv /home/mike/.config/sublime-text-3/Packages/User/* /home/mike/gitlab/dotfiles/.config/sublime-text-3/Packages/User
rm -vf /home/mike/gitlab/dotfiles/.config/sublime-text-3/Packages/User/Package\ Control.merged-ca-bundle

echo "[$(date)] 'bkup-subl': synced Sublime config files" >> ~/.local/log/cron.log
notify-send "bkup-subl':" "$(date)\n\nSynced Sublime config files"

