#!/bin/bash
#-------------------------------------------------------------------------------
# Script Name:      dbackup.sh
# Description:      Backup to Dropbox.
# Dependencies:     rsync, Dropbox
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
# backup files
rsync -avz \
        $HOME/gitlab/dotfiles/.aliases \
        $HOME/.nanorc \
        $HOME/exclude_me.txt \
        $HOME/exclude_usb.txt \
        $HOME/gitlab/dotfiles/.zshrc $HOME/Dropbox/Laconia-cfg-files/

# backup document folders
#rsync -avz $HOME/Documents $HOME/Dropbox/Laconia-docs/

# backup bin and config folders
rsync -avz --exclude-from='/home/mike/exclude_me.txt' \
        $HOME/.nano \
        $HOME/src $HOME/Dropbox/Laconia-cfg-files/

# backup .config subfolders
rsync -avz --exclude-from='/home/mike/exclude_me.txt' \
        $HOME/gitlab/dotfiles/.config/alacritty \
        $HOME/gitlab/dotfiles/.config/bottom \
        $HOME/gitlab/dotfiles/.config/kak \
        $HOME/gitlab/dotfiles/.config/kitty \
        $HOME/gitlab/dotfiles/.config/helix \
        $HOME/gitlab/dotfiles/.config/micro \
        $HOME/gitlab/dotfiles/.config/plank \
        $HOME/gitlab/dotfiles/.config/ranger \
        $HOME/gitlab/dotfiles/.config/rofi \
        $HOME/gitlab/dotfiles/.config/starship \
        $HOME/gitlab/dotfiles/.config/vifm \
        $HOME/gitlab/dotfiles/.config/vimiv $HOME/Dropbox/Laconia-cfg-files/.config/

# backup .nb.sh
rsync -avz \
        $HOME/.nb.sh $HOME/Dropbox/


# post backup section
echo "[$(date)] 'dbackup.sh': synced (~/dropbox/laconia-cfg-files)" >> /home/mike/.local/log/cron.log

notify-send "dbackup.sh:" "$(date)\n\ncrontab: backups ran"

