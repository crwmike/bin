#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      ubackup.sh
# Description:      Backup home directory with exclude from ~/exclude_me.txt
# Dependencies:     rsync
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
set -e

if grep -qs '/run/media/mike/ubackup ' /proc/mounts; then
    # USB drive is mounted, run backup
    rsync -az --delete-during --exclude-from='/home/mike/exclude_usb.txt' \
        /home/mike /run/media/mike/ubackup/LaconiaBU
    echo "[$(date)] 'ubackup.sh': backup home (/run/media/mike/ubackup)" >> ~/.local/log/cron.log
    rm -rf /run/media/mike/ubackup/LaconiaBU/mike/.local/share/Trash/*
    notify-send "ubackup.sh:" "$(date)\n\nend of sync (/run/media/mike/ubackup/)"
else
    # USB drive is not mounted, abort
    echo "[$(date)] 'ubackup.sh': 'ERROR': /run/media/mike/ubackup/ not mounted." >> ~/.local/log/cron.log
    notify-send -u critical "ubackup.sh:" "$(date)\n\nSYNC FAILED; /run/media/mike/ubackup/ not mounted"
fi

