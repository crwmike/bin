
#!/bin/bash
#-------------------------------------------------------------------------------
# Script Name:      bu-music.sh
# Description:      Backup ~/Music/*
# Source:           ~/Music/
# Destination:      /run/media/mike/ubackup/Music/
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
set -e
cp -rf /home/mike/Music/* /run/media/mike/ubackup/Music/

echo "[$(date)] 'bu-music.sh': backup music" >> /home/mike/.local/log/cron.log
