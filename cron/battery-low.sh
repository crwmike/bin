#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      battery-low.sh
# Description:      Warn when battry is low
# Dependencies:     systemd
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
# Add to crontab, run every 2 minutes.
# */2  *  *  *  *  /home/mike/bin/battery-low.sh &
#-------------------------------------------------------------------------------
CAPACITY=$(cat /sys/class/power_supply/BAT0/capacity)
STATE=$(cat /sys/class/power_supply/BAT0/status)

#### uncomment to test script ####
#/usr/bin/espeak "Battery OK, ${CAPACITY} percent, ${STATE}"
#echo "[$(date)] 'battery-low.sh': this is a test" >> /home/mike/.local/log/cron.log

if [ $CAPACITY -lt 20 ]; then
    /usr/bin/notify-send "Low Battery!" "$(date)\n\nbattery level is ${CAPACITY}% ${STATE}"
    /usr/bin/espeak "low battery, ${CAPACITY} percent, ${STATE}"
fi

