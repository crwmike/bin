#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      crontab-copy.sh
# Description:      Copy crontab to ~/bin/crontab-copy/
# Dependencies:     cron, crontab
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------

/usr/bin/crontab -l > /home/mike/bin/crontab-copy/cron-mike.txt
sleep 2
echo "[$(date)] 'crontab-copy.sh': copied crontab (~/bin/crontab-copy)" >> /home/mike/.local/log/cron.log

