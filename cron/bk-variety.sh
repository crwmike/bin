#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      bk-variety.sh
# Description:      Backup Variety downloaded wallpapers
# Dependencies:     rsync, variety
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
# Check if ubackup is mounted
if grep -qs '/run/media/mike/ubackup' /proc/mounts; then
    # USB drive is mounted, run backup
    rsync -avz /home/mike/.config/variety /run/media/mike/ubackup/
    echo "$(date) : 'bk-variety': synced files/dirs (/run/media/mike/ubackup/variety)" >> ~/.local/log/cron.log
    date >>  /run/media/mike/ubackup/variety/user.log
else
    # USB drive is not mounted, abort
    echo "[$(date)] bk-variety: SYNC FAILED; /run/media/mike/ubackup/ not mounted." >> ~/.local/log/cron.log
fi

