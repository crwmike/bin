#!/bin/bash
#-------------------------------------------------------------------------------
# Script Name:      empty-trash.sh
# Description:      Empty Trash (~/.local/share/Trash/)
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------

# empty current user's trash (~/.local/share/Trash)
LSTRASH=$(ls -ARF ~/.local/share/Trash)
rm -rfv ~/.local/share/Trash/files/*
rm -rfv ~/.local/share/Trash/info/*
echo "[$(date)] 'empty-trash.sh': empty trash directory" >> /home/mike/.local/log/cron.log
notify-send "empty-trash.sh:" "$(date)\n\ncrontab: empty Trash"
echo -n $'\a'

