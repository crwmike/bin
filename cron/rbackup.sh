#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      rbackup    
# Description:      Backup home directory
# Dependencies:     rsync
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
set -e

#-------------------------------------------------------------------------------
# backup selected folder in my home folder
#-------------------------------------------------------------------------------
rsync -az --delete-during \
    $HOME/Dropbox \
    $HOME/gitlab \
    /backup/mike/ 2>/dev/null

echo "[$(date)] 'rbackup.sh': backups home (/backup/mike)" >> /home/mike/.local/log/cron.log

