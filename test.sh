#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      test.sh
# Description:      Testing
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
for i in {30..1}
do 
    echo -en "Shutdown in ${i} second(s).\r"
    sleep 1
    done
echo 
