#!/usr/bin/env bash
# --------------------------------
# ---  STATUS:  Uptime         ---
# ---           hoastname      ---
# ---           Memory         ---
# ---           Disk Usage     ---
# ---           Connections    ---
# ---           Top Processes  ---
# --------------------------------
#
CCAT='ccat --bg=dark --color=always
        -G Keyword=white
        -G Type=white
        -G Punctuation=darkblue
        -G Plaintext=white
        -G String=teal
        -G Decimal=purple'

section() {
    echo
    tput setaf 81
    figlet -f future $1
    tput sgr0
}

section2() {
    echo -e "\n"
    title=" $1 "
    vbox="│"
    tl="╭"; tr="╮"; bl="╰"; br="╯"
    edge=$(echo "$title" | sed 's/./─/g')
    echo ${tl}${edge}${tr} | $CCAT
    echo -e ${vbox}${title}${vbox} | $CCAT
    echo ${bl}${edge}${br} | $CCAT
}

uhost() {
    #tput cuu 1
    section "Uptime"
    uptime --pretty
    printf "since "
    uptime --since
    #tput cuu 1
}

mfree() {
    section "Memory Usage"
    free -hw
    #tput cuu 1
}

udisk() {
    section "Disk Usage"
    /bin/df -h -x efivarfs -x tmpfs
    #tput cuu 1
}

users() {
    section "Logins"
    who -H
    #tput cuu 1
}

pubip() {
    section "Network"
    nmcli -c no device
    #tput cuu 1
}

proc() {
    section "Top Processes"
    #top -b | head -n 20 | tail -n 11
    ps aux | sort -rk 4,4 | head -n 5 | awk '{printf "%7s       %-7s %5s      %-50s\n ", $2, $1, $4, $11}'
    echo
}

if [ $# -eq 0 ]; then
    uhost
    users
    mfree
    udisk
    pubip
    echo
    exit
fi

echo "STATUS: Displays system information.
        Uptime
        Logins
        Memory Usage
        Disk Usage
        Network
        Top Processes"
