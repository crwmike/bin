BSD Games
=========

__BSD Games__ is a collection of the classic text based games distributed with 
some BSDs like FreeBSD and NetBSD.

The included games are, alphabetically:

* _adventure_       an exploration game
* _arithmetic_      quiz on simple arithmetic
* _atc_             air traffic controller game
* _backgammon_      the game of backgammon
* _banner_          print large banner on printer
* _battlestar_      a tropical adventure game
* _bcd_             reformat input as punch cards, paper tape or morse code
* _boggle_          word search game
* _caesar_          decrypt caesar cyphers
* _canfield_        the solitaire card game canfield
* _cfscores_        show scores for canfield
* _cribbage_        the card game cribbage
* _fish_            play Go Fish
* _gomoku_          game of 5 in a row
* _hangman_         Computer version of the game hangman
* _hunt_            a multi-player multi-terminal game
* _huntd_           hunt daemon, back-end for hunt game
* _mille_           play Mille Bornes
* _monop_           Monopoly game
* _morse_           reformat input as punch cards, paper tape or morse code
* _number_          convert Arabic numerals to English
* _phantasia_       an interterminal fantasy game
* _pom_             display the phase of the moon
* _ppt_             reformat input as punch cards, paper tape or morse code
* _primes_          generate primes
* _quiz_            random knowledge tests
* _rain_            animated raindrops display
* _random_          random lines from a file or random numbers
* _robots_          fight off villainous robots
* _rot13_           rot13 encrypt/decrypt
* _sail_            multi-user wooden ships and iron men
* _snake_           display chase game
* _teachgammon_     learn to play backgammon
* _tetris-bsd_      the game of tetris
* _trek_            trekkie game
* _wargames_        shall we play a game?
* _worm_            Play the growing worm game
* _worms_           animate worms on a display terminal
* _wtf_             translates acronyms for you
* _wump_            hunt the wumpus in an underground cave
