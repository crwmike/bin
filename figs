#!/bin/bash
#-------------------------------------------------------------------------------
# Script Name:      figs
# Description:      List figlet aliases.
# Dependencies:     figlet
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
figHelp() {
    echo -e "\e[97mFIGS\e[0m"
    echo
    echo -e "Usage: figs [\e[92mOPTION\e[0m]"
    echo
    echo -e "list figlet aliases"
    echo
    echo -e "    \e[92m<none>          \e[0mrun figs"
    echo -e "    \e[92m-h, --help      \e[0mprints this help"
    echo -e "    \e[92m-l, --list      \e[0mrun figlet on the list"
    echo
}

figFonts=(
    "ansishadow"
    "bloody"
    "bigmoney"
    "calvin"
    "chunky"
    "future"
    "larry3d"
    "miniwi"
    "maxiwi"
    "pagga"
    "rebel"
    "roman"
    "rusto"
    "slant"
    "small"
    "smdots"
    "smblock"
    "straight"
)

if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
    figHelp
    exit
fi

if [ "$1" == "--list" ] || [ "$1" == "-l" ]; then
    echo
    for figFonts in "${figFonts[@]}"; do
        tput setaf 69
        figlet -f $figFonts $figFonts
        tput sgr0
        echo $figFonts
    done
    echo
fi

tput setaf 69
figlet -f pagga "figlet aliases"
tput sgr0
for figFonts in "${figFonts[@]}"; do
    printf "%-8s\n" "${figFonts}"
done | column -c 70


