#!/usr/bin/env bash
#   __________________________
#  /  cowsay.sh               \
#  |                          |
#  |  Displays words in all   |
#  \  available cowsay cows.  /
#   --------------------------
#          \   ^__^
#           \  (oo)\_______
#              (__)\       )\/\
#                  ||----w |
#                  ||     ||
#  --'---"-^--^---'---.--,---^-----'--^---,--
# cowsay.sh: displays iWords in all available cowsay cowss.
iWords="This is a test, this is only a test."

list_cows() {
    #tput setaf 39
    echo -e "\e[38;5;39m-------------------------------------------------\e[0m"
    echo -e "\e[32mcowsay \e[0m-f \e[33m<cowname>"
    echo -e "\e[38;5;39m-------------------------------------------------\e[0m"
    ls /usr/share/cowsay/cows/
}

# If words are given as an arg, use those words.
if [ ! -z $1 ]
then
    iWords="$*"
fi

# List all figlet fonts
for f in /usr/share/cowsay/cows/*
do
    tput setaf 39
    fs=$(basename "$f")
    fname=${fs%%.cow}
    cowsay -f "$fname" "$iWords" 2> /dev/null
    tput setaf 7
    echo "$fname"
done
list_cows

