#!/usr/bin/env python3
#############################
###  MAGIC RAM INSTALLER  ###
#############################
import sys
import time
import threading
import itertools

print (50 * '#')
print (50 * '-')
print ("\033[36m         WELCOME TO MAGIC RAM INSTALLER\033[0m")
print (50 * '-')
print (50 * '#')
time.sleep(1)
print ("MIT License, v. 1.4")
time.sleep(1)
print()

def main():
    spinner = itertools.cycle(['-', '/', '|', '\\'])
    t_end = time.time() + 15
    ram = str(input("\033[36mEnter the amount of RAM you want to install\033[0m:  "))
    print()
    print("\033[92mGathering System Information...\033[0m")
    print()
    time.sleep(5.0)
    print("Wow...your photos are...interesting...")
    print()
    time.sleep(3.0)
    print("Installing RAM Now...")
    print()

    while time.time() < t_end:
          sys.stdout.write(next(spinner))  # write the next character
          sys.stdout.flush()               # flush stdout buffer (actual character display)
          time.sleep(1.0)
          sys.stdout.write('\b')

    else:
          print("Installation Complete!")
          print()
          time.sleep(2.0)
          print("You may want to hide those photos in the future")
          time.sleep(2.0)
          print()
          print("\033[93mPLEASE REBOOT TO MAKE CHANGES PERMANENT\033[0m")
          time.sleep(2.0)
          print()
main()

