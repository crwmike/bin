#!/bin/bash
#-------------------------------------------------------------------------------
# Script name:      netapps
# Description:      Menu of installed network applications
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
TX="\e[0m"
ROOT="\e[91m"
CM="\e[33m"
quit_msg="\e[93m"
NU="\e[36m"

function sudo_message() {
    clear
    echo -e "\n  \e[92m#####################\n  ##  \e[97mSUDO PASSWORD  \e[92m##\n  #####################\n\e[0m"
}

function no_cmd() {
    echo "netapps:

    =================================
     $(date)
    =================================
     ERROR! selection #${runme}
     command or file not found!
    =================================

    " | less -e

}

function press_me() {
    echo
    tput setaf 0
    tput setab 3
    read -n 1 -s -r -p "Press any key to exit"
    tput sgr0
}

function draw_line() {
    tput setaf 4
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
}

function app_menu() {
    clear
    tput setaf 116 && figlet -f pagga "network applications"
    draw_line
    echo -e " ${NU} 1. ${CM}arp-scan      ${ROOT}root${TX}: The ARP scanner."
    echo -e " ${NU} 2. ${CM}bandwhich     ${ROOT}root${TX}: CLI utility for displaying current network utilization."
    echo -e " ${NU} 3. ${CM}bmon          ${TX}A powerful network bandwidth monitoring and debugging tool."
    echo -e " ${NU} 4. ${CM}cbm           ${TX}Displays in real time the network traffic speed."
    echo -e " ${NU} 5. ${CM}iftop         ${ROOT}root${TX}: A network bandwidth monitoring tool for Linux."
    echo -e " ${NU} 6. ${CM}iptraf        ${ROOT}root${TX}: An IP network statistic utility."
    echo -e " ${NU} 7. ${CM}mtr           ${TX}A tool that combines the functionality of ping and traceroute."
    echo -e " ${NU} 8. ${CM}nethogs       ${ROOT}root${TX}: Monitor per process network bandwidth usage in Linux."
    echo -e " ${NU} 9. ${CM}netstat       ${ROOT}root${TX}: Prints network connections, routing tables..."
    echo -e " ${NU} a. ${CM}nload         ${TX}Displays the current network usage."
    echo -e " ${NU} b. ${CM}nload -m      ${TX}Show multiple devices at the same time, no graph."
    echo -e " ${NU} c. ${CM}slurm         ${TX}Yet another network load monitor."
    echo -e " ${NU} d. ${CM}speedtest     ${TX}CLI tool for testing internet bandwidth using speedtest.net."
    echo -e " ${NU} Q. ${quit_msg}quit          ${TX}Quit this menu."
    draw_line
    echo -e "${CM}Enter your choice${quit_msg}:  "
    draw_line
    tput cup 19 20
}

#-------------------------------------------------------------------------------
# execution
#-------------------------------------------------------------------------------

while true;
do
    app_menu
    read -r -n 1 runme
    runme=${runme,,}
    tput sgr0

    case "$runme" in
        1) sudo_message; sudo arp-scan --localnet --ignoredups || no_cmd && press_me ;;
        2) sudo_message; sudo bandwhich -n || no_cmd ;;
        3) echo; bmon || no_cmd ;;
        4) echo; cbm || no_cmd ;;
        5) sudo_message; sudo iftop -i wlan0 || no_cmd ;;
        6) echo; echo; sudo iptraf-ng || no_cmd ;;
        7) echo; mtr google.com || no_cmd ;;
        8) sudo_message; sudo nethogs || no_cmd ;;
        9) sudo_message; sudo netstat -tupl || no_cmd && press_me ;;
        a) echo; nload device wlan0 || no_cmd ;;
        b) echo; nload -m || no_cmd ;;
        c) echo; slurm -i wlan0 || no_cmd ;;
        d) echo; echo; speedtest || no_cmd && press_me ;;
        q|x|z) echo; echo; break ;;
    esac
done

# ------------------------------------------------------------------------------
# ---- Links                                                                ----
# ------------------------------------------------------------------------------
# https://github.com/imsnif/bandwhich
# https://github.com/tgraf/bmon
# https://github.com/matttbe/ifstat
# https://github.com/iptraf-ng/iptraf-ng
# https://github.com/raboof/nethogs 
# https://github.com/rolandriegel/nload
# https://www.speedtest.net/apps/cli
