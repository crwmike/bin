#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      vinfo
# Description:      Simple fetch script
# Dependencies:     -
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
cln="\e[38;5;255m"
hdr="\e[38;5;228m"
txt="\e[0m"
itm="\e[96m"
draw_line() {
    tput setaf 254
    for i in {1..60}; do
        echo -n "-"
    done
    tput sgr0
    echo
}

# Terminal
trim() {
    set -f
    # shellcheck disable=2048,2086
    set -- $*
    printf '%s\n' "${*//[[:space:]]/ }"
    set +f
}

get_ppid() {
    # Get parent process ID of PID.
    ppid="$(grep -i -F "PPid:" "/proc/${1:-$PPID}/status")"
    ppid="$(trim "${ppid/PPid:}")"
    printf "%s" "$ppid"
}

get_process_name() {
    # Get PID name.
    name="$(< "/proc/${1:-$PPID}/comm")"
    printf "%s" "$name"
}

get_mem(){
    while IFS=":" read -r a b; do
        case "$a" in
            "MemTotal") mem_used="$((mem_used+=${b/kB}))"; mem_total="${b/kB}" ;;
            "Shmem") mem_used="$((mem_used+=${b/kB}))"  ;;
            "MemFree" | "Buffers" | "Cached" | "SReclaimable")
                mem_used="$((mem_used-=${b/kB}))"
            ;;
        esac
    done < /proc/meminfo

    mem_used="$((mem_used / 1024))"
    mem_total="$((mem_total / 1024))"
    memory="${mem_used}${mem_label:-MiB} / ${mem_total}${mem_label:-MiB}"
    echo "$memory"
}

get_term(){
    while [[ -z "$term" ]]; do
        parent="$(get_ppid "$parent")"
        [[ -z "$parent" ]] && break
        name="$(get_process_name "$parent")"

        case "${name// }" in
            "${SHELL/*\/}"|*"sh"|"screen"|"su"*) ;;

            "login"*|*"Login"*|"init"|"(init)")
                term="$(tty)"
            ;;

            "ruby"|"1"|"systemd"|"sshd"*|"python"*|"USER"*"PID"*|"kdeinit"*|"launchd"*)
                break
            ;;

            "gnome-terminal-") term="gnome-terminal" ;;
            *"nvim")           term="Neovim Terminal" ;;
            *"NeoVimServer"*)  term="VimR Terminal" ;;
            *"tmux"*)          term="tmux" ;;
            *)                 term="${name##*/}" ;;
        esac
    done

    # Log that the function was run.
    term_run=1
    echo "$term"
}

############################################################
echo -e "\n${hdr}Hardware Information${cln}:${txt}"
draw_line
############################################################
echo -en "${itm}Model${cln}:    ${txt}"
cat /sys/devices/virtual/dmi/id/product_name

echo -en "${itm}CPU${cln}:      ${txt}"
grep model\ name /proc/cpuinfo | head -n 1 | awk '{for (i=4; i<NF; i++) printf $i " "; if (NF >= 4) print $NF; }'

echo -en "${itm}GPU${cln}:      ${txt}"
lspci | grep ' VGA ' | awk '{for (i=7; i<NF; i++) printf $i " "; if (NF >= 7) print $NF; }'

echo -en "${itm}RAM${cln}:      ${txt}"
get_mem

echo -en "${itm}Uptime${cln}:   ${txt}"
uptime --pretty

############################################################
echo -e "\n${hdr}Software Information${cln}:${txt}"
draw_line
############################################################
echo -en "${itm}OS${cln}:       ${txt}"
lsb_release -d 2>/dev/null | awk '{print $2 " " $3 " " $4}'

echo -en "${itm}Kernel${cln}:   ${txt}"
uname --kernel-name --kernel-release

echo -en "${itm}DE/WM${cln}:    ${txt}"
echo "$DESKTOP_SESSION"

echo -en "${itm}Terminal${cln}: ${txt}"
get_term

echo -en "${itm}Shell${cln}:    ${txt}"
echo "$SHELL" | awk -F "/" '{print $NF}'

echo -en "${itm}Packages${cln}: ${txt}"
PACM=$(pacman -Qq --color never | wc -l)
FLAT=$(flatpak list | wc -l)
echo -e "${PACM} (pacman), ${FLAT} (flatpak)"
echo
