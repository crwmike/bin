#!/usr/bin/env bash
#░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
#░░█▀▀░▀█▀░▀█▀░░░█▀█░█░░░▀█▀░█▀█░█▀▀░█▀▀░█▀▀░░
#░░█░█░░█░░░█░░░░█▀█░█░░░░█░░█▀█░▀▀█░█▀▀░▀▀█░░
#░░▀▀▀░▀▀▀░░▀░░░░▀░▀░▀▀▀░▀▀▀░▀░▀░▀▀▀░▀▀▀░▀▀▀░░
#░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
alias g='git'
alias gaa='git add --all'
alias gapa='git add --patch'
alias gb='git branch'
alias gba='git brance -a'
alias gbd='git brance -b'
alias gbd='git brance -d'
alias gco='git checkout'
alias gc='git commit -v'
alias gcmsg='git commit -m'
alias gds='git diff --staged'
alias gdw='git diff --word-diff'
alias glg='git log --stat'
alias glog='git log --graph'
alias gll='git log --oneline --decorate --graph'
alias gm='git merge'
alias gl='git pull'
alias gp='git push'
alias gru='git reset --'
alias grs='git restore'
alias grm='git rm'
alias gsh='git show'
alias gst='git status'
alias gsb='git status -sb'
