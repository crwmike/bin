#!/usr/bin/env bash

CCAT='ccat --bg=dark --color=always
            -G Keyword=white
            -G Type=white
            -G Punctuation=blue
            -G Plaintext=white
            -G String=white
            -G Decimal=white'

roundBox() {
    echo
    title=" $1 "
    vbox="│"
    tl="╭"; tr="╮"; bl="╰"; br="╯"
    edge=$(echo "$title" | sed 's/./─/g')
    echo ${tl}${edge}${tr} | $CCAT
    echo -e ${vbox}${title}${vbox} | $CCAT
    echo ${bl}${edge}${br} | $CCAT
}

hashBox() {
    echo
    title="##  $1  ##"
    edge=$(echo "$title" | sed 's/./#/g')
    echo "$edge" | $CCAT
    echo -e "$title" | $CCAT
    echo "$edge" | $CCAT
}

colonBox() {
    echo
    title="::  $1  ::"
    edge=$(echo "$title" | sed 's/./:/g')
    echo "$edge" | $CCAT
    echo -e "$title" | $CCAT
    echo "$edge" | $CCAT
}

WORD="TEST"
if [ ! -z $1 ]
then
    WORD="$*"
fi

hashBox "$WORD"
colonBox "$WORD"
roundBox "$WORD"
echo
