#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script name:      shebang.sh
# Description:      Vim/Neovim shortcht to insert a shebang 
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
function help_text() {
    /bin/cat << HELP
shebang.sh: vim/neovim shortcut for writing a shebang

        -h  --help      this help screen
        -b  --bash      insert a bash shebang
        -p  --python    insert a python shebang
        -P  --perl      insert a perl shebang

HELP
    exit
}

NORMAL="\e[0m"

case "$1" in
    "") help_text ;;
    --bash|-b) echo "#!/usr/bin/env bash" ;;
    --python|-p) echo "#!/usr/bin/env python" ;;
    --perl|-P) echo "#!/usr/bin/env perl" ;;
    --help|-h) help_text ;;
esac
script-head.sh --add
