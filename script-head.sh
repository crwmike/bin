#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script Name:      script-head.sh
# Description:      vim/nvim shortcut to add header to shell scripts
# Dependencies:     vim nvim
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
function help_text() {
    /bin/cat << HELP
script-head.sh: vim/neovim shortcut for inserting a header

        -h  --help      this help screen
        -a  --add       insert header in the current script

HELP
    exit
}

function insert_header() {
    echo "#-------------------------------------------------------------------------------"
    echo "# Script Name:      -"
    echo "# Description:      -"
    echo "# Dependencies:     -"
    echo "# GitLab:           https://www.gitlab.com/crwmike/bin"
    echo "# Contributors:     Michael Grosen"
    echo "#-------------------------------------------------------------------------------"
}

if [ "$1" == "--add" ] || [ "$1" == "-a" ]; then
    insert_header
    exit
else
    help_text
fi
