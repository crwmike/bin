#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Script name:      utils
# Description:      CLI utility menu using rofi.
# GitLab:           https://www.gitlab.com/crwmike/bin
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
myTerm=kitty

menu() {
    printf "atop:         [root] A CLI monitoring tool.\n"
    printf "bandwhich:    A CLI utility for displaying networks.\n"
    printf "bmon:         A network monitoring and debugging tool.\n"
    printf "btop:         Resource monitor that shows usage and stats.\n"
    printf "cbm:          Displays in real time the network traffic speed.\n"
    printf "collectl:     An all-in-one performance monitoring tool.\n"
    printf "diskonaut:    A terminal disk space navigator.\n"
    printf "gdu:          Disk usage analyzer.\n"
    printf "glances:      A curses-based monitoring tool.\n"
    printf "htop:         An interactive process viewer.\n"
    printf "iftop:        [root] A network bandwidth monitor.\n"
    printf "kmon:         Linux Kernel Manager and Activity Monitor.\n"
    printf "ncdu:         A disk usage analyzer.\n"
    printf "mtr:          A tool that combines ping and tracerout.\n"
    printf "nethogs:      [root] Monitor Per Process Network Bandwidth Usage.\n"
    printf "nload:        Displays the current network usage.\n"
    printf "nload.m:      Show multiple devices at the same time, no graph.\n"
    printf "nmon:         Analyze and monitor linux system performance.\n"
    printf "sar:          (System Activity Report) CPU, Memory, and I/O usage.\n"
    printf "slurm:        Yet another network load monitor.\n"
    printf "stacer:       Linux System Optimizer and Monitoring.\n"
    printf "s-tui:        CPU stress and monitoring utility.\n"
    printf "top:          Displays processor activity.\n"
    printf "vmstat:       Report virtual memory statistics.\n"
    printf "zenith:       In terminal graphical metrics.\n"
}

main() {
    choice=$(menu | rofi -p ' CLI Utils ' -columns 1 -dmenu | cut -d: -f1)

    case $choice in
        atop) $myTerm -e sudo atop -y ;;
        bandwhich) $myTerm -e sudo bandwhich ;;
        bmon) $myTerm -e bmon -p wlo1,lo ;;
        btop) $myTerm -e btop ;;
        cbm) $myTerm -e cbm ;;
        collectl) $myTerm -e collectl ;;
        diskonaut) $myTerm -e diskonaut /home/mike ;;
        gdu) $myTerm -e gdu ;;
        glances) $myTerm -e glances -2 --process-short-name ;;
        htop) $myTerm -e htop ;;
        iftop) $myTerm -e sudo iftop -i wlan0 ;;
        kmon) $myTerm -e kmon -c cyan ;;
        ncdu) $myTerm -e ncdu ;;
        mtr) $myTerm -e mtr google.com ;;
        nethogs) $myTerm -e sudo nethogs ;;
        nload) $myTerm -e nload device wlan0 ;;
        nload.m) $myTerm -e nload -m ;;
        nmon) $myTerm -e nmon ;;
        sar) $myTerm -e sar 1 ;;
        slurm) $myTerm -e slurm -i wlan0 ;;
        stacer) stacer ;;
        s-tui) $myTerm -e s-tui ;;
        top) $myTerm -e top ;;
        vmstat) $myTerm -e vmstat -a 1 ;;
        vtop) $myTerm -e vtop ;;
        zenith) $myTerm -e zenith ;;
    esac
}

main

